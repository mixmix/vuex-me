import { merge, cloneDeep } from 'lodash-es'

export function State (state, opts = {}) {
  return merge(cloneDeep(state), opts)
}

export function Mutations (state, mutations) {
  const activeMutations = {}

  for (const [mutationName, mutationFn] of Object.entries(mutations)) {
    activeMutations[mutationName] = (input) => mutationFn(state, input)
  }

  return activeMutations
}

export function Getters (state, getters) {
  const activeGetters = {}

  const getterNames = Object.keys(getters)
  let count = 0

  while (getterNames.length) {
    if (++count > 1000) throw new Error(`Getters helper stuck on: ${getterNames.join(', ')}`)
    const getterName = getterNames.shift()

    try {
      const result = getters[getterName](state, activeGetters)
      // if it explodes, it was probably dependant on some other getters
      // so if it doesn't add it to the activeGetters

      if (typeof result === 'function') activeGetters[getterName] = result
      else {
        Object.defineProperty(activeGetters, getterName, {
          get: () => getters[getterName](state, activeGetters)
        })
      }
    } catch (err) {
      console.log(err)
      // not ready yet!
      getterNames.push(getterName)
    }
  }

  return activeGetters
}

export default function Module (module, ...args) {
  if (typeof module === 'function') module = module(...args)

  if (!module.mutations) module.mutations = {}
  if (!module.getters) module.getters = {}

  const state = State(module.state)
  const mutations = Mutations(state, module.mutations)

  return {
    state,
    mutations,
    commit (mutationName, input) {
      if (!mutations[mutationName]) throw new Error(`unknown mutationName: ${mutationName}`)

      return mutations[mutationName](input)
    },
    getters: Getters(state, module.getters)
  }
}
