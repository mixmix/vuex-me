# Vuex Me

## Usage

```js
import Module from 'vuex-me'
import test from 'tape'

import module from '../../src/store/modules/graph.js'

test('vuex/graph', t => {
  const { state, getters, commit, mutations } = Module(module)


  mutations.addRelationship({ parent: 'Mum1', child: 'Son' })
  commit('addRelationship', { parent: 'Mum2', child: 'Son' })

  t.deepEqual(state.children['Mum1'], ['Son'])

  t.deepEqaual(getters.getParents('Son'), ['Mum1', 'Mum2'])
  t.deepEqaual(getters.onlyChilden, ['Son']) // calculated as this property is called

  t.equal(

})
```

## Notes

- sets up an initial state
- wires up mutations so they can be called simply (as function or "commit")
- wires up getters so that they
    - as functions
    - as properties which are calculated as the property is accessed
    - work when a getter depends on another getter
