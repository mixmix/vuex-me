import test from 'tape'
import Module, { Getters } from '../index.js'

test('Getters', t => {
  const state = {
    focus: 'Grandma',
    childLinks: {
      Grandma: {
        Dad: 'birth',
        Aunt: 'birth'
      },
      Grandad: {
        Dad: 'birth',
        Aunt: 'adopted'
      },
      Dad: {
        Me: 'birth'
      }
    }
  }

  const _getters = {
    focus: (state) => state.focus,
    childIds: (state) => (id) => Object.keys(state.childLinks[id] || {}),
    familyTree (state, getters) {
      return buildTree(getters.focus)

      function buildTree (id) {
        return {
          id,
          children: getters.childIds(id)
            .map(childId => buildTree(childId))
        }
      }
    }
  }

  const getters = Getters(state, _getters)

  t.equal(getters.focus, 'Grandma', 'simple')
  state.focus = 'Grandad'
  t.equal(getters.focus, 'Grandad', 'simple (updates after state mutation)')

  t.deepEqual(getters.childIds('Dad'), ['Me'], 'method getters')

  const expected = {
    id: 'Grandad',
    children: [
      {
        id: 'Dad',
        children: [
          {
            id: 'Me',
            children: []
          }
        ]
      },
      {
        id: 'Aunt',
        children: []
      }
    ]
  }
  t.deepEqual(getters.familyTree, expected, 'getters which call getters')

  const module = Module({
    state,
    getters: _getters
  })
  t.deepEqual(module.getters.familyTree, expected, 'module.getters which call getters')

  t.end()
})
