import test from 'tape'
import Module, { Mutations } from '../index.js'

test('Mutations', t => {
  const _module = {
    state: { count: 1 },
    mutations: {
      increment (state, n = 1) {
        state.count += n
      }
    }
  }

  const mutations = Mutations(_module.state, _module.mutations)

  mutations.increment()
  t.equal(_module.state.count, 2)

  const module = Module(_module)

  module.mutations.increment()
  t.equal(module.state.count, 3, 'mutation function')

  module.commit('increment', 3)
  t.equal(module.state.count, 6, 'commit')

  t.end()
})
