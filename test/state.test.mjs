import test from 'tape'
import Module, { State } from '../index.js'

test('State', t => {
  const initialState = {
    a: 1,
    b: { name: 'danae' }
  }

  let state

  state = State(initialState)
  state.b.name = 'trevor'
  t.deepEqual(
    initialState,
    {
      a: 1,
      b: { name: 'danae' }
    },
    'State creates a copy so initialState is never mutated'
  )

  state = State(initialState, { a: 2, b: { age: 39 } })
  t.deepEqual(
    state,
    {
      a: 2,
      b: { name: 'danae', age: 39 }
    },
    'merge over-rides'
  )

  const module = Module({ state: initialState })
  t.deepEqual(module.state, initialState, 'module.state - deeply equal')
  t.notEqual(module.state, initialState, 'module.state - deeply clones')

  t.end()
})
